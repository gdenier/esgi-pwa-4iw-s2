import page from '/node_modules/page/page.mjs';
import { openDB } from 'idb';
import Task from "./views/task";
import { conf } from "/config.js";
import checkConnectivity from './network.js';

var onLine = true;

(async () => {

    const app = document.querySelector('#app main');

    document.addEventListener('conectivity-changed', e => {
        const root = document.documentElement;
        document.offline = !e.detail;
        if (e.detail) {
            console.log('Back online');
            reconciliateDB(db);
            root.style.setProperty('--app-main-color', '#25b99a');
            onLine = true;
        } else {
            console.log('Offline mode');
            root.style.setProperty('--app-main-color', '#c9c9c9');
            onLine = false;
        }
    });
    checkConnectivity({});

    const db = await openDB("toDoList", 1, {
        upgrade(db, oldVersion, newVersion, transaction) {
            db.createObjectStore('tasks');
        }
    });

    // pull all data from api
    const data = await getAllTasksFromAPI(db);
    
    const homeNode = app.querySelector('[page=home]');

    const pages = [
        homeNode
    ];

    page('/', async param => {
        const view = await import('./views/home.js');
        const Home = view.default;
        Home(homeNode, data, db);

        pages.forEach(page => page.removeAttribute('active'));
        homeNode.setAttribute('active', true);
    });

    page();

    document.getElementById('add').addEventListener('click', function() {
        var value = document.getElementById('item').value;
        if (value) {
            addTask(value, db);
        }
    });

    document.getElementById('item').addEventListener('keydown', function (e) {
        var value = this.value;
        if ((e.code === 'Enter' || e.code === 'NumpadEnter') && value) {
            addTask(value, db);
        }
    });
})();

function addTask(text, db) {
    const task = new Task(text, false, db);
    task.save();
    document.querySelector('.todo').appendChild(task.generate());
}

async function getAllTasksFromAPI(db){
    const data = await db.getAll('tasks') || [];

    if (onLine) {
        const result = await fetch(`${conf.api_url}/tasks`).then(response => {
            return response;
        });
        const data = await result.json();
    
        data.forEach(item => {
            db.put('tasks', {"id": item.id, "text": item.text, "done": item.done}, item.id);
        });
    }

    return data;
}

async function reconciliateDB(db){
    const result = await fetch(`${conf.api_url}/tasks`).then(response => {
        return response;
    });
    const dataAPI = await result.json();

    let reconciliated = [];
    
    await dataAPI.forEach(async item => {
        const itemIDB = await db.get('tasks', item.id) || {};
        if (JSON.stringify(itemIDB) === JSON.stringify({})) {
            fetch(`${conf.api_url}/tasks/${item.id}`, {
                method: 'DELETE'
            });
        } else {
            fetch(`${conf.api_url}/tasks/${item.id}`, {
                method: 'PUT',
                headers: new Headers({'content-type': 'application/json'}),
                body: JSON.stringify(itemIDB)
            });
            reconciliated.push(itemIDB.id);
        }
    });
    
    const dataIDB = await db.getAll('tasks') || [];
    dataIDB.forEach(item => {
        if(!reconciliated.includes(item.id)) {
            fetch(`${conf.api_url}/tasks`, {
                method: 'POST',
                headers: new Headers({'content-type': 'application/json'}),
                body: JSON.stringify(item)
            })
        }
    });

}