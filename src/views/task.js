import { conf } from "/config.js";

export default class Task {
    
    constructor(text, done, db, id) {
        this.id = id ? id : Date.now();
        this.text = text;
        this.done = done;
        this.db = db;
        
        this.template = document.createElement('li');
        this.template.classList.add('task');
        this.template.innerHTML = `
            <p></p>
            <div class="buttons">
                <button class="remove"></button>
                <button class="complete"></button>
            </div>
        `;
    }

    generate() {
        const text = this.template.querySelector('p');
        text.innerText = this.text;


        const supress = () => {
            const parent = this.template.parentNode;

            const root = document.documentElement;
            const onLine = root.style.getPropertyValue('--app-main-color') === '#25b99a';

            if (onLine) {
                fetch(`${conf.api_url}/tasks/${this.id}`, {
                    method: 'DELETE'
                }).then(() => {
                    this.db.delete('tasks', this.id);
                }).then(() => {
                    parent.removeChild(this.template);
                });
            } else {
                this.db.delete('tasks', this.id).then(() => {
                    parent.removeChild(this.template);
                });
            }

        }

        const remove = this.template.querySelector('.remove');
        remove.innerHTML = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve"><rect class="noFill" width="22" height="22"/><g><g><path class="fill" d="M16.1,3.6h-1.9V3.3c0-1.3-1-2.3-2.3-2.3h-1.7C8.9,1,7.8,2,7.8,3.3v0.2H5.9c-1.3,0-2.3,1-2.3,2.3v1.3c0,0.5,0.4,0.9,0.9,1v10.5c0,1.3,1,2.3,2.3,2.3h8.5c1.3,0,2.3-1,2.3-2.3V8.2c0.5-0.1,0.9-0.5,0.9-1V5.9C18.4,4.6,17.4,3.6,16.1,3.6z M9.1,3.3c0-0.6,0.5-1.1,1.1-1.1h1.7c0.6,0,1.1,0.5,1.1,1.1v0.2H9.1V3.3z M16.3,18.7c0,0.6-0.5,1.1-1.1,1.1H6.7c-0.6,0-1.1-0.5-1.1-1.1V8.2h10.6V18.7z M17.2,7H4.8V5.9c0-0.6,0.5-1.1,1.1-1.1h10.2c0.6,0,1.1,0.5,1.1,1.1V7z"/></g><g><g><path class="fill" d="M11,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6s0.6,0.3,0.6,0.6v6.8C11.6,17.7,11.4,18,11,18z"/></g><g><path class="fill" d="M8,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6c0.4,0,0.6,0.3,0.6,0.6v6.8C8.7,17.7,8.4,18,8,18z"/></g><g><path class="fill" d="M14,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6c0.4,0,0.6,0.3,0.6,0.6v6.8C14.6,17.7,14.3,18,14,18z"/></g></g></g></svg>';
        remove.addEventListener('click', supress);


        const done = () => {
            const parent = this.template.parentNode;

            const root = document.documentElement;
            const onLine = root.style.getPropertyValue('--app-main-color') === '#25b99a';
            if (onLine) {
                fetch(`${conf.api_url}/tasks/${this.id}`, {
                    method: 'PATCH',
                    headers: new Headers({'content-type': 'application/json'}),
                    body: '{"done": true}'
                }).then(() => {
                    this.done = true;
                    this.db.put('tasks', {"id": this.id, "text": this.text, "done": this.done}, this.id);
                }).then(() => {
                    this.template.querySelector('.complete').removeEventListener('click', done);
                    this.template.querySelector('.complete').addEventListener('click', undo);
                    document.querySelector('#completed').appendChild(this.template);
                });
            } else {
                this.done = true;
                this.db.put('tasks', {"id": this.id, "text": this.text, "done": this.done}, this.id).then(() => {
                    this.template.querySelector('.complete').removeEventListener('click', done);
                    this.template.querySelector('.complete').addEventListener('click', undo);
                    document.querySelector('#completed').appendChild(this.template);
                });
            }

        }

        const undo = () => {
            const parent = this.template.parentNode;

            const root = document.documentElement;
            const onLine = root.style.getPropertyValue('--app-main-color') === '#25b99a';

            if (onLine) {
                fetch(`${conf.api_url}/tasks/${this.id}`, {
                    method: 'PATCH',
                    headers: new Headers({'content-type': 'application/json'}),
                    body: '{"done": false}'
                }).then(() => {
                    this.done = false;
                    this.db.put('tasks', {"id": this.id, "text": this.text, "done": this.done}, this.id);
                }).then(() => {
                    this.template.querySelector('.complete').removeEventListener('click', undo);
                    this.template.querySelector('.complete').addEventListener('click', done);
                    document.querySelector('#todo').appendChild(this.template);
                });
            } else {
                this.done = false;
                this.db.put('tasks', {"id": this.id, "text": this.text, "done": this.done}, this.id).then(() => {
                    this.template.querySelector('.complete').removeEventListener('click', undo);
                    this.template.querySelector('.complete').addEventListener('click', done);
                    document.querySelector('#todo').appendChild(this.template);
                });
            }


        };

        const complete = this.template.querySelector('.complete');
        complete.innerHTML = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve"><rect y="0" class="noFill" width="22" height="22"/><g><path class="fill" d="M9.7,14.4L9.7,14.4c-0.2,0-0.4-0.1-0.5-0.2l-2.7-2.7c-0.3-0.3-0.3-0.8,0-1.1s0.8-0.3,1.1,0l2.1,2.1l4.8-4.8c0.3-0.3,0.8-0.3,1.1,0s0.3,0.8,0,1.1l-5.3,5.3C10.1,14.3,9.9,14.4,9.7,14.4z"/></g></svg>';
        complete.addEventListener('click', done);

        return this.template;
    }

    save() {
        const root = document.documentElement;
        const onLine = root.style.getPropertyValue('--app-main-color') === '#25b99a';

        let newTask = {"id": this.id, "text": this.text, "done": this.done};

        if (onLine) {
            fetch(`${conf.api_url}/tasks`, {
                method: 'POST',
                headers: new Headers({'content-type': 'application/json'}),
                body: JSON.stringify(newTask)
            }).then(() => {
                this.db.put('tasks', newTask, this.id);
            });
        } else {
            this.db.put('tasks', newTask, this.id);
        }
        
    }
    
};