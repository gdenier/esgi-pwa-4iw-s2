import Task from "./task";

export default function Home(page, data, db) {
    page.innerHTML = `
    <div class="container">
        <ul class="todo" id="todo"></ul>

        <ul class="todo" id="completed"></ul>
    </div>
    `;
    
    data.forEach(item => {
        const task = new Task(item.text, item.done, db, item.id);
        page.querySelector(item.done ? '#completed' : '#todo').appendChild(task.generate());
    });
};